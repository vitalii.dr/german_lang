const SCNOMEN = [
  {
    artickel: "das",
    nomen: "Abfahren",
    mehrzahl: "Abfahren",
    single: "вивіз/обїзд/обкатка",
    many: "вивіз/обїзд/обкатка",
  },
  {
    artickel: "das",
    nomen: "Bett",
    mehrzahl: "Betten",
    single: "ліжко",
    many: "ліжко"
  },
  {
    artickel: "das",
    nomen: "Bier",
    mehrzahl: "Biere",
    single: "пиво",
    many: "пиво"
  },
  {
    artickel: "das",
    nomen: "Bild",
    mehrzahl: "Bilder",
    single: "картина/зображення/образ/портрет/фото/символ/статуя/малюнок",
    many: "картина/зображення/образ/портрет/фото/символ/статуя/малюнок"
  },
  {
    artickel: "das",
    nomen: "Brot",
    mehrzahl: "Brote",
    single: "хліб",
    many: "хліб"
  },
  {
    artickel: "das",
    nomen: "Buch",
    mehrzahl: "Bücher",
    single: "книга",
    many: "книги"
  },
  {
    artickel: "das",
    nomen: "Büro",
    mehrzahl: "Büros",
    single: "бюро",
    many: "бюро"
  },
  {
    artickel: "das",
    nomen: "Cafe",
    mehrzahl: "Cafes",
    single: "кава",
    many: "кава"
  },
  {
    artickel: "das",
    nomen: "Ding",
    mehrzahl: "Dinge",
    single: "річ",
    many: "речі"
  },
  {
    artickel: "das",
    nomen: "Dorf",
    mehrzahl: "Dörfer",
    single: "село",
    many: "села"
  },
  {
    artickel: "das",
    nomen: "Eis",
    mehrzahl: "Eis",
    single: "лід/морозиво",
    many: "лід/морозиво"
  },
  {
    artickel: "das",
    nomen: "Ende",
    mehrzahl: "Enden",
    single: "кінець",
    many: "кінці"
  },
  {
    artickel: "das",
    nomen: "Foto",
    mehrzahl: "Fotos",
    single: "фото",
    many: "фото"
  },
  {
    artickel: "das",
    nomen: "Fenster",
    mehrzahl: "Fenster",
    single: "вікно",
    many: "вікна"
  },
  {
    artickel: "das",
    nomen: "Festnetz",
    mehrzahl: "Festnetze",
    single: "стаціонарний телефон",
    many: "стаціонарні телефони"
  },
  {
    artickel: "das",
    nomen: "Fräulein",
    mehrzahl: "Damen",
    single: "панянка",
    many: "панянки"
  },
  {
    artickel: "das",
    nomen: "Fragezeichen",
    mehrzahl: "Fragezeichen",
    single: "знак",
    many: "знаки питання"
  },
  {
    artickel: "das",
    nomen: "Gebiet",
    mehrzahl: "Gebiete",
    single: "область/район/поле/округ/сфера/зона",
    many: "область/район/поле/округ/сфера/зона"
  },
  {
    artickel: "das",
    nomen: "Gegenteil",
    mehrzahl: "Gegenteile",
    single: "антонім",
    many: "антоніми"
  },
  {
    artickel: "das",
    nomen: "Gehalt",
    mehrzahl: "Gehälter",
    single: "частка/зарплатня/утримання",
    many: "частка/зарплатня/утримання"
  },
  {
    artickel: "das",
    nomen: "Gehören",
    mehrzahl: "-",
    single: "належність, користь",
    many: "належність, користь"
  },
  {
    artickel: "das",
    nomen: "Gehör",
    mehrzahl: "Gehöre",
    single: "Слух",
    many: "Слух"
  },
  {
    artickel: "das",
    nomen: "Geschäft",
    mehrzahl: "Geschäfte",
    single: "магазин/діло",
    many: "магазин/діло"
  },
  {
    artickel: "das",
    nomen: "Geschirr",
    mehrzahl: "Geschirr",
    single: "посуд",
    many: "посуд"
  },
  {
    artickel: "das",
    nomen: "Getränk",
    mehrzahl: "Getränke",
    single: "напій",
    many: "напій"
  },
  {
    artickel: "das",
    nomen: "Gleis",
    mehrzahl: "Gleise",
    single: "залізнична дорога/платформа",
    many: "залізнична дорога/платформа"
  },
  {
    artickel: "das",
    nomen: "Glück",
    mehrzahl: "Glücke",
    single: "щастя",
    many: "щастя"
  },
  {
    artickel: "das",
    nomen: "Gold",
    mehrzahl: "Gold",
    single: "золото",
    many: "золото"
  },
  {
    artickel: "das",
    nomen: "Gramm",
    mehrzahl: "Gramme",
    single: "грам",
    many: "грами"
  },
  {
    artickel: "das",
    nomen: "Grundzahlwort",
    mehrzahl: "Grundzahlwörter",
    single: "кількісний числівник",
    many: "числівники"
  },
  {
    artickel: "das",
    nomen: "Haar",
    mehrzahl: "Haare",
    single: "волосок",
    many: "волосся"
  },
  {
    artickel: "das",
    nomen: "Haus",
    mehrzahl: "Hause",
    single: "будинок",
    many: "будинки"
  },
  {
    artickel: "das",
    nomen: "Hähnchen",
    mehrzahl: "Hähnchen",
    single: "курка",
    many: "кури"
  },
  {
    artickel: "das",
    nomen: "Hängen",
    mehrzahl: "-",
    single: "висіння/повішання",
    many: "висіння/повішання"
  },
  {
    artickel: "das",
    nomen: "Hemd",
    mehrzahl: "Hemden",
    single: "сорочка",
    many: "сорочки"
  },
  {
    artickel: "das",
    nomen: "Holz",
    mehrzahl: "Hölzer",
    single: "деревина",
    many: "деревина"
  },
  {
    artickel: "das",
    nomen: "Jackett",
    mehrzahl: "Jacketts",
    single: "куртка/піджак",
    many: "куртка/піджак"
  },
  {
    artickel: "das",
    nomen: "Kilo",
    mehrzahl: "Kilos",
    single: "кілограм",
    many: "кілограми"
  },
  {
    artickel: "das",
    nomen: "Kind",
    mehrzahl: "Kinder",
    single: "дитина",
    many: "діти"
  },
  {
    artickel: "das",
    nomen: "Kino",
    mehrzahl: "Kinos",
    single: "кіно",
    many: "кіно"
  },
  {
    artickel: "das",
    nomen: "Kreuz",
    mehrzahl: "Keuze",
    single: "хрест, перехрестя",
    many: "хрести"
  },
  {
    artickel: "das",
    nomen: "Kreuzen",
    mehrzahl: "Kreuzen",
    single: "роспяття, обмін",
    many: "роспяття, обмін"
  },
  {
    artickel: "das",
    nomen: "Land",
    mehrzahl: "Lände",
    single: "земля/країна/суша/село",
    many: "земля/країна/суша/село"
  },
  {
    artickel: "das",
    nomen: "Lebensmittel",
    mehrzahl: "Lebensmittel",
    single: "їжа",
    many: "їжа"
  },
  {
    artickel: "das",
    nomen: "Mädchen",
    mehrzahl: "Mädchen",
    single: "дівчина",
    many: "дівчата"
  },
  {
    artickel: "das",
    nomen: "Maß",
    mehrzahl: "Maße",
    single: "міра",
    many: "міри"
  },
  {
    artickel: "das",
    nomen: "Messer",
    mehrzahl: "Messer",
    single: "ніж",
    many: "ножі"
  },
  {
    artickel: "das",
    nomen: "Niveau",
    mehrzahl: "Niveaus",
    single: "рівень/стандарт",
    many: "рівні/стандарти"
  },
  {
    artickel: "das",
    nomen: "Nomen",
    mehrzahl: "Nomen",
    single: "іменник",
    many: "іменники"
  },
  {
    artickel: "das",
    nomen: "Ohr",
    mehrzahl: "Ohren",
    single: "вухо",
    many: "вуха"
  },
  {
    artickel: "das",
    nomen: "Öhr",
    mehrzahl: "Öhren",
    single: "вушко",
    many: "вушка"
  },
  {
    artickel: "das",
    nomen: "Öl",
    mehrzahl: "Öle",
    single: "олія",
    many: "олії"
  },
  {
    artickel: "das",
    nomen: "Papier",
    mehrzahl: "Papiere",
    single: "папір",
    many: "папір"
  },
  {
    artickel: "das",
    nomen: "Passen",
    mehrzahl: "Passen",
    single: "пригодність",
    many: "пригодність"
  },
  {
    artickel: "das",
    nomen: "Pech",
    mehrzahl: "Peche",
    single: "смола, невдача",
    many: "смоли, невдачі"
  },
  {
    artickel: "das",
    nomen: "Possessivpronomen",
    mehrzahl: "Possessivpronomen",
    single: "присвійний займенник",
    many: "займенники"
  },
  {
    artickel: "das",
    nomen: "Pronomen",
    mehrzahl: "Pronomen",
    single: "займенник",
    many: "займенники"
  },
  {
    artickel: "das",
    nomen: "Putzen",
    mehrzahl: "Putzen",
    single: "прибирання",
    many: "прибирання"
  },
  {
    artickel: "das",
    nomen: "Rätsel",
    mehrzahl: "Rätsel",
    single: "казка",
    many: "казки"
  },
  {
    artickel: "das",
    nomen: "Regal",
    mehrzahl: "Regale",
    single: "полка/стелаж/регалія/реал",
    many: "полка/стелаж/регалія/реал"
  },
  {
    artickel: "das",
    nomen: "Sacken",
    mehrzahl: "Sacken",
    single: "провисання",
    many: "провисання"
  },
  {
    artickel: "das",
    nomen: "Schranktrocken",
    mehrzahl: "Schranktrocken",
    single: "сушильний шкаф",
    many: "сушильний шкаф"
  },
  {
    artickel: "das",
    nomen: "Schriftsteller",
    mehrzahl: "Schriftsteller",
    single: "письменник",
    many: "письменники"
  },
  {
    artickel: "das",
    nomen: "Schwein",
    mehrzahl: "Schweine",
    single: "свиня",
    many: "свині"
  },
  {
    artickel: "das",
    nomen: "Spielzeug",
    mehrzahl: "Spielzeuge",
    single: "іграшка",
    many: "іграшки"
  },
  {
    artickel: "das",
    nomen: "Springen",
    mehrzahl: "Springen",
    single: "стрибок",
    many: "стрибки"
  },
  {
    artickel: "das",
    nomen: "Spülen",
    mehrzahl: "Spülen",
    single: "промивання",
    many: "промивання"
  },
  {
    artickel: "das",
    nomen: "Streitgespräch",
    mehrzahl: "Streitgespräche",
    single: "дискусія",
    many: "дискусії"
  },
  {
    artickel: "das",
    nomen: "Substantiv",
    mehrzahl: "Substantive",
    single: "іменник",
    many: "іменники"
  },
  {
    artickel: "das",
    nomen: "Telefon",
    mehrzahl: "Telefone",
    single: "телефон",
    many: "телефони"
  },
  {
    artickel: "das",
    nomen: "Tier",
    mehrzahl: "Tiere",
    single: "тварина",
    many: "тварини"
  },
  {
    artickel: "das",
    nomen: "T-Shirt",
    mehrzahl: "T-Shirts",
    single: "футболка",
    many: "футболки"
  },
  {
    artickel: "das",
    nomen: "Trocknen",
    mehrzahl: "Trocknen",
    single: "висихання, обезвоження",
    many: "висихання, обезвоження"
  },
  {
    artickel: "das",
    nomen: "Verb",
    mehrzahl: "Verben",
    single: "дієслово",
    many: "дієслово"
  },
  {
    artickel: "das",
    nomen: "Vieh",
    mehrzahl: "Vieh",
    single: "худоба",
    many: "худоба"
  },
  {
    artickel: "das",
    nomen: "Volk",
    mehrzahl: "Völker",
    single: "народ",
    many: "народи"
  },
  {
    artickel: "das",
    nomen: "Wählen",
    mehrzahl: "Wählen",
    single: "вибір",
    many: "вибір"
  },
  {
    artickel: "das",
    nomen: "Wasser",
    mehrzahl: "Wasser",
    single: "вода",
    many: "води"
  },
  {
    artickel: "das",
    nomen: "Wort",
    mehrzahl: "Wörten",
    single: "слово",
    many: "слова"
  },
  {
    artickel: "das",
    nomen: "Zahlwort",
    mehrzahl: "Zahlwörter",
    single: "числівник",
    many: "числівники"
  },
  {
    artickel: "das",
    nomen: "Ziel",
    mehrzahl: "Ziele",
    single: "ціль",
    many: "цілі"
  },
  {
    artickel: "das",
    nomen: "Zimmer",
    mehrzahl: "Zimmer",
    single: "кімната",
    many: "кімнати"
  },
  {
    artickel: "das",
    nomen: "Zusatzblatt",
    mehrzahl: "Zusatzblätter",
    single: "додатковий лист",
    many: "додаткові листи"
  }
];
