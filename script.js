function writeInfo(name, data, clr) {
  let table = document.getElementById(name).getElementsByTagName('tbody')[0];
  console.log("Write data to table", data, table);
  data.forEach(word => {
    let count = 0;
    let row = table.insertRow();
    if (clr != ""){
      row.setAttribute( 'class', clr);
    }
    Object.keys(word).forEach(function(key) {
      let k = row.insertCell(count);
      k.innerHTML = word[key];
      count++;
    });
  });
};

function myFunction() {
  writeInfo("schverbs", SCHVERBS, "");
  writeInfo("mnnomen", MNNOMEN, "man");
  writeInfo("wbnomen", WBNOMEN, "woman");
  writeInfo("scnomen", SCNOMEN, "middle");
}

myFunction();
